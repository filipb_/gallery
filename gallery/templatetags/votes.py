from django import template
from ..models import PhotoVote, CurrentPhotoVote


register = template.Library()


@register.assignment_tag(takes_context=True)
def photo_upvoted(context, *args, **kwargs):
    photo = context['photo']
    if kwargs['user'].is_authenticated():
        return PhotoVote.objects \
            .filter(user=kwargs['user'], photo=photo) \
            .exists()
    return False


@register.assignment_tag(takes_context=True)
def current_photo_upvoted(context, *args, **kwargs):
    current_photo = context['current_photo']
    if kwargs['user'].is_authenticated():
        return CurrentPhotoVote.objects \
            .filter(user=kwargs['user'], current_photo=current_photo) \
            .exists()
    return False
