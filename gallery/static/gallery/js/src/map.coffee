initMap = (target) ->

    worldSource = new ol.source.OSM

    worldLayer = new ol.layer.Tile
        source: worldSource

    map = new ol.Map
        target: target
        layers: [worldLayer]
        controls: new ol.Collection()
        view: new ol.View
            center: lonLatToCoords(getLocation())
            zoom: 14


initProfileMap = (target) ->
    lon = parseFloat($('#' + target).attr('lon'))
    lat = parseFloat($('#' + target).attr('lat'))
    coords = lonLatToCoords([lon, lat])

    worldSource = new ol.source.OSM
    worldLayer = new ol.layer.Tile
        source: worldSource

    markersSource = new ol.source.Vector({})
    markersLayer = new ol.layer.Vector
        source: markersSource

    map = new ol.Map
        target: target
        layers: [worldLayer, markersLayer]
        controls: new ol.Collection()
        view: new ol.View
            center: coords
            zoom: 14

    # Add a marker
    feature = new ol.Feature({})
    feature.setGeometry(new ol.geom.Point(coords))
    style = new ol.style.Style
        image: new ol.style.Icon
            src: '/static/gallery/img/marker.png'
            anchor: [0.5, 1]
    feature.setStyle(style)
    markersSource.addFeature(feature)


initMapPickerOnForm = (target, lonInputName, latInputName) ->
    callbackOnSelection = (lonlat) ->
        $("input[name=#{lonInputName}]").val(lonlat[0])
        $("input[name=#{latInputName}]").val(lonlat[1])

    lon = $("input[name=#{lonInputName}]").val()
    lat = $("input[name=#{latInputName}]").val()
    if lon and lat
        initial = [parseFloat(lon), parseFloat(lat)]
    else
        initial = null

    initMapPicker(target, callbackOnSelection, initial)


# Creates a map which is used to select a location. When the map is clicked
# a marker is placed on it and the coordinates of the selected position are
# passed to the provided callback function.
#
# target: the id of a div to be used as a map container.
# callback: a function which will be passed a [lon, lat] array as an argument.
# initial: the initial position of the marker as [lon, lat] or null.
initMapPicker = (target, callback, initial) ->
    worldSource = new ol.source.OSM
    worldLayer = new ol.layer.Tile
        source: worldSource

    markersSource = new ol.source.Vector({})
    markersLayer = new ol.layer.Vector
        source: markersSource

    map = new ol.Map
        target: target
        layers: [worldLayer, markersLayer]
        controls: new ol.Collection()
        view: new ol.View
            center: lonLatToCoords(getLocation())
            zoom: 14

    interaction = new ol.interaction.Select
        condition: ol.events.condition.singleClick
        layers: [worldLayer]
        multi: false

    map.addInteraction(interaction)

    placeMarker = (coords) ->
        feature = new ol.Feature({})
        feature.setGeometry(new ol.geom.Point(coords))
        style = new ol.style.Style
            image: new ol.style.Icon
                src: '/static/gallery/img/marker.png'
                anchor: [0.5, 1]
        feature.setStyle(style)

        markersSource.clear()
        markersSource.addFeature(feature)

    if initial
        coords = lonLatToCoords(initial)
        placeMarker(coords)
        map.getView().setCenter(coords)

    interaction.on('select', (e) ->
        lonlat = coordsToLonLat(e.mapBrowserEvent.coordinate)
        callback(lonlat)
        placeMarker(e.mapBrowserEvent.coordinate)
    )


initLocationPreviewMap = (target) ->
    lon = parseFloat($('#' + target).attr('lon'))
    lat = parseFloat($('#' + target).attr('lat'))
    coords = lonLatToCoords([lon, lat])

    worldSource = new ol.source.OSM
    worldLayer = new ol.layer.Tile
        source: worldSource

    markersSource = new ol.source.Vector({})
    markersLayer = new ol.layer.Vector
        source: markersSource

    map = new ol.Map
        target: target
        layers: [worldLayer, markersLayer]
        controls: new ol.Collection()
        view: new ol.View
            center: coords
            zoom: 14

    # Add a marker
    feature = new ol.Feature({})
    feature.setGeometry(new ol.geom.Point(coords))
    style = new ol.style.Style
        image: new ol.style.Icon
            src: '/static/gallery/img/marker.png'
            anchor: [0.5, 1]
    feature.setStyle(style)
    markersSource.addFeature(feature)
