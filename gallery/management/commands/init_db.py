import os
from django.core.management.base import BaseCommand
from django.core.files import File
from ...models import User, Photo, CurrentPhoto, Comment


text =  """
It all began with the forging of the Great Rings. Three were given to the Elves;
immortal, wisest and fairest of all beings. Seven, to the Dwarf Lords, great
miners and craftsmen of the mountain halls. And nine, nine rings were gifted to
the race of Men, who above all else desire power. For within these rings was
bound the strength and the will to govern over each race. But they were all of
them deceived, for another ring was made. In the land of Mordor, in the fires of
Mount Doom, the Dark Lord Sauron forged in secret, a master ring, to control all
others. And into this ring he poured all his cruelty, his malice and his will to
dominate all life. One ring to rule them all. One by one, the free peoples of
Middle Earth fell to the power of the Ring. But there were some who resisted. A
last alliance of men and elves marched against the armies of Mordor, and on the
very slopes of Mount Doom, they fought for the freedom of Middle-Earth. Victory
was near, but the power of the ring could not be undone. It was in this moment,
when all hope had faded, that Isildur, son of the king, took up his father's
sword. And Sauron, enemy of the free peoples of Middle-Earth, was defeated. The
Ring passed to Isildur, who had this one chance to destroy evil forever, but the
hearts of men are easily corrupted. And the ring of power has a will of its own.
It betrayed Isildur, to his death. And some things that should not have been
forgotten were lost. History became legend. Legend became myth. And for two and
a half thousand years, the ring passed out of all knowledge. Until, when chance
came, the ring ensnared a new bearer. The ring came to the creature Gollum, who
took it deep into the tunnels under the Misty Mountains, and there it consumed
him. The ring gave to Gollum unnatural long life. For five hundred years it
poisoned his mind; and in the gloom of Gollum's cave, it waited. Darkness crept
back into the forests of the world. Rumor grew of a shadow in the East, whispers
of a nameless fear, and the Ring of Power perceived. Its time had now come. It
abandoned Gollum. But then something happened that the Ring did not intend. It
was picked up by the most unlikely creature imaginable. A hobbit, Bilbo Baggins,
of the Shire. For the time will soon come when hobbits will shape the fortunes
of all... 
"""


class Command(BaseCommand):

    help = 'Fills the database with dev data'

    def get_file_path(self, filename):
        directory = os.path.dirname(os.path.abspath(__file__))
        relative_path = '../../../_misc/img/%s' % filename
        return os.path.abspath(os.path.join(directory, relative_path))

    def create_users(self):
        print('Adding users:')

        admin = User.objects.create_superuser('admin', 'admin@example.com', 'admin')
        print(admin)

        user = User.objects.create_user('emptyuser', 'emptyuser@example.com', 'emptyuser')
        print(user)

        self.users = []

        user = User.objects.create_user('test', 'test@example.com', 'test')
        user.userdata.lat = 50.061389
        user.userdata.lon = 19.938333
        user.userdata.display_location = True
        user.userdata.about_me = 'Lorem ipsum dolor sit amet.'
        user.userdata.save()
        self.users.append(user)
        print(user)

        for i in range(10):
            a = 'test%s' % i
            user = User.objects.create_user(a, a + '@example.com', a)
            user.userdata.lat = [50, -50, None][i % 3]
            user.userdata.lon = [50, -50, None][i % 3]
            user.userdata.display_location = [True, False][i % 2]
            user.userdata.about_me = ['Lorem ipsum dolor sit amet', ''][i % 2]
            user.userdata.save()
            print(user)
            self.users.append(user)

    def create_photos(self):
        print('Adding photos:')

        photo_path = self.get_file_path('dev_photo.jpg')

        i = 0
        for description in ['Lorem ipsum dolor sit amet', '']:
            for number in [0, 1, 2, 5, 10]:
                p = Photo()
                p.lat = 50.061389
                p.lon = 19.938333
                p.description = description
                p.user = self.users[0]
                with open(photo_path, 'rb') as f:
                    p.image.save('%s.jpg' % i, File(f), save=True)
                print(p)

                self.create_comments(p, 3 * number)
                self.create_current_photos(p, number)

                i += 1

    def create_comments(self, photo, number):
        for i in range(number):
            comment = Comment()
            comment.user = self.users[i%len(self.users)]
            comment.photo = photo
            comment.text = text[:1000]
            comment.save()
            print(comment)

    def create_current_photos(self, photo, number):
        for j in range(number):
            current_photo = CurrentPhoto(user=self.users[j], photo=photo)
            current_photo_path = self.get_file_path('dev_current_photo_%s.jpg' % (j % 2 + 1))
            with open(current_photo_path, 'rb') as f:
                current_photo.image.save('%s_%s.jpg' % (photo.pk, j), File(f), save=True)
            print(current_photo)

    def handle(self, *args, **options):
        self.create_users()
        self.create_photos()
