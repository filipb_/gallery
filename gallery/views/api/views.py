from django.utils.translation import ugettext as _
from ...forms.core import AddCurrentPhotoForm
from ...models import CurrentPhoto, Photo, PhotoVote, CurrentPhotoVote
from ..mixins import LoginRequiredMixin
from .base import APIView
from .errors import BadRequestAPIError, UnauthorizedAPIError


class AddCurrentPhotoView(LoginRequiredMixin, APIView):

    def handle_form(self, form):
        # Confirm that the photo exists
        photo = Photo.objects.filter(pk=self.kwargs['pk']).first()
        if photo is None:
            raise BadRequestAPIError(message=_('The historical photo does not exist.'))

        # Confirm that the current photo does not exist
        current_photo = CurrentPhoto.objects.filter(user=self.request.user,
                                                    photo=photo).first()
        if current_photo is not None:
            raise BadRequestAPIError(message=_('You have already uploaded a current photo of this location.'))

        if not form.is_valid():
            raise BadRequestAPIError(message=_('Invalid image.'))

        current_photo = form.save(commit=False)

        # Confirm that the aspect ratio is correct
        photo_ratio = photo.image.width / photo.image.height
        current_photo_ratio = current_photo.image.width / current_photo.image.height
        if abs(1 - photo_ratio/current_photo_ratio) > 0.005:
            raise BadRequestAPIError(message=_('The uploaded image has an invalid aspect ratio.'))

        current_photo.user = self.request.user
        current_photo.photo = photo
        current_photo.save()
        return current_photo

    def post(self, request, *args, **kwargs):
        form = AddCurrentPhotoForm(request.POST, request.FILES)
        current_photo = self.handle_form(form)
        return current_photo.json()


class PhotoVoteView(APIView):

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            raise UnauthorizedAPIError

        if not 'upvote' in self.request.POST:
            raise BadRequestAPIError(message=_('Parameter missing.'))

        photo = Photo.objects.filter(pk=self.kwargs['pk']).first()
        if photo is None:
            raise BadRequestAPIError(message=_('The historical photo does not exist.'))

        if self.request.POST['upvote'] == 'true':
            if not PhotoVote.objects.filter(user=self.request.user, photo=photo).exists():
                pv = PhotoVote(user=self.request.user, photo=photo)
                pv.save()
            return {'upvoted': True}
        else:
            pv = PhotoVote.objects.filter(user=self.request.user, photo=photo).delete()
            return {'upvoted': False}


class CurrentPhotoVoteView(APIView):

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            raise UnauthorizedAPIError

        if not 'upvote' in self.request.POST or not 'id' in self.request.POST:
            raise BadRequestAPIError(message=_('Parameter missing.'))

        current_photo = CurrentPhoto.objects.filter(pk=self.request.POST['id']).first()
        if current_photo is None:
            raise BadRequestAPIError(message=_('The photo does not exist.'))

        if self.request.POST['upvote'] == 'true':
            if not CurrentPhotoVote.objects \
                .filter(user=self.request.user, current_photo=current_photo) \
                .exists():
                pv = CurrentPhotoVote(user=self.request.user, current_photo=current_photo)
                pv.save()
            return {'upvoted': True}
        else:
            pv = CurrentPhotoVote.objects \
                .filter(user=self.request.user, current_photo=current_photo) \
                .delete()
            return {'upvoted': False}
