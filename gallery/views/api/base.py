from django.http import JsonResponse
from django.views.generic.base import View
from .errors import APIError, MethodNotAllowedAPIError


class APIView(View):
    """A view which makes sure that properly structured JSON is always
    returned.
    """

    def prepare_response(self, exception):
        response_data = {
            'error_code': exception.error_code,
            'message': exception.message,
        }
        return (response_data, exception.status_code)

    def dispatch(self, request, *args, **kwargs):
        try:
            attr_name = request.method.lower()
            if request.method.lower() in self.http_method_names and hasattr(self, attr_name):
                response_data = getattr(self, attr_name)(request, *args, **kwargs)
                status_code = 200
            else:
                raise MethodNotAllowedAPIError(methods=self._allowed_methods())

        # Handle the exceptions thorown on purpose.
        except APIError as e:
            response_data, status_code = self.prepare_response(e)

        # Handle other exceptions.
        except Exception as e:
            raise
            response_data, status_code = self.prepare_response(APIError())

        return JsonResponse(response_data, status=status_code)
