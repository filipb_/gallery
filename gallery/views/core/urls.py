from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add/$', views.AddPhotoView.as_view(), name='add_photo'),
    url(r'^add/(?P<pk>([0-9]+))/$', views.AddCurrentPhotoView.as_view(), name='add_current_photo'),
    url(r'^remove_current/(?P<pk>([0-9]+))/$', views.RemoveCurrentPhotoView.as_view(), name='remove_current_photo'),
    url(r'^photo/(?P<pk>([0-9]+))/$', views.PhotoView.as_view(), name='photo'),
    url(r'^photo/(?P<pk>([0-9]+))/compare/(?P<compare>([0-9]+))$', views.PhotoView.as_view(), name='photo_compare'),
    url(r'^user/(?P<pk>([0-9]+))/$', views.ProfileView.as_view(), name='profile'),
]
