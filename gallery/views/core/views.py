from django.core.paginator import EmptyPage, PageNotAnInteger
from django.db.models import Count
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from ...forms.core import AddPhotoForm, CommentForm
from ...models import CurrentPhoto, Photo, User
from ..mixins import LoginRequiredMixin
from ..paginator import CustomPaginator


def index(request):
    return render(request, "gallery/core/index.html")


class AddPhotoView(LoginRequiredMixin, FormView):
    template_name = 'gallery/core/add_photo.html'
    form_class = AddPhotoForm

    def form_valid(self, form):
        photo = form.save(commit=False)
        photo.user = self.request.user
        photo.save()
        return redirect(photo)


class AddCurrentPhotoView(LoginRequiredMixin, TemplateView):
    template_name = 'gallery/core/add_current_photo.html'

    def get_context_data(self, **kwargs):
        context = super(AddCurrentPhotoView, self).get_context_data(**kwargs)
        context['photo'] = Photo.objects \
            .filter(pk=self.kwargs['pk']) \
            .first()
        context['current_photo'] = CurrentPhoto.objects \
            .filter(user=self.request.user, photo=context['photo'])\
            .first()
        return context


class RemoveCurrentPhotoView(LoginRequiredMixin, TemplateView):
    template_name = 'gallery/core/remove_current_photo.html'

    def process_request(self):
        self.current_photo = CurrentPhoto.objects \
            .filter(pk=self.kwargs['pk']) \
            .first()
        self.errors = []
        if not self.current_photo:
            self.errors.append(_('This photo does not exist.'))
        else:
            if self.request.user != self.current_photo.user:
                self.errors.append(_('This photo was not uploaded by you.'))

    def get_context_data(self, **kwargs):
        context = super(RemoveCurrentPhotoView, self).get_context_data(**kwargs)
        context['current_photo'] = self.current_photo
        context['errors'] = self.errors
        return context

    def get(self, request, *args, **kwargs):
        self.process_request()
        return super(RemoveCurrentPhotoView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.process_request()
        if not self.errors:
            self.current_photo.delete()
            return redirect(self.current_photo.photo)
        return super(RemoveCurrentPhotoView, self).post(request, *args, **kwargs)


class PhotoView(FormView):
    template_name = 'gallery/core/photo.html'
    form_class = CommentForm
    comments_per_page = 10
    current_photos_per_page = 5

    def get_photo(self):
        self.object = Photo.objects.get(pk=self.kwargs['pk'])
        return self.object

    def get_your_current_photo(self):
        if self.request.user.is_authenticated():
            return CurrentPhoto.objects \
                .filter(user=self.request.user, photo=self.object) \
                .first()
        return None


    def get_comments(self):
        page = self.request.GET.get('comments_page')
        objects = self.object.comment_set.order_by('-created').all()
        comments = CustomPaginator(objects, self.comments_per_page)
        try:
            return comments.page(page)
        except PageNotAnInteger:
            return comments.page(1)
        except EmptyPage:
            return comments.page(comments.num_pages)

    def get_current_photos(self):
        page = self.request.GET.get('photos_page')
        objects = self.object.currentphoto_set \
            .annotate(score=Count('currentphotovote')) \
            .order_by('-score') \
            .all()

        photos = CustomPaginator(objects, self.current_photos_per_page)
        try:
            return photos.page(page)
        except PageNotAnInteger:
            return photos.page(1)
        except EmptyPage:
            return photos.page(photos.num_pages)

    def get_context_data(self, **kwargs):
        context = super(PhotoView, self).get_context_data(**kwargs)
        context['photo'] = self.get_photo()
        context['current_photos'] = self.get_current_photos()
        context['your_current_photo'] = self.get_your_current_photo()
        context['comments'] = self.get_comments()
        if 'compare' in self.kwargs:
            context['compare_to'] = CurrentPhoto.objects \
                .filter(photo=self.object, pk=self.kwargs['compare']) \
                .first()
            context['should_compare'] = True
        return context

    def form_valid(self, form):
        if self.request.user.is_authenticated():
            comment = form.save(commit=False)
            comment.user = self.request.user
            comment.photo = self.get_photo()
            comment.save()
        return redirect(self.get_photo())


class ProfileView(DetailView):
    template_name = 'gallery/core/profile.html'
    model = User
    context_object_name = 'profile'
