from django.contrib.auth import REDIRECT_FIELD_NAME
from django.utils.http import is_safe_url
from django.shortcuts import resolve_url


def get_redirect_url(request, redirect_to, field_name=REDIRECT_FIELD_NAME):
    """If a query string parameter with a defined name is set it returns the URL
    provided in it, otherwise it returns the url provided as a parameter to this
    function.

    request: an HttpRequest request object.
    redirect_to: an url, a model or a view name which will be returned if a GET
                 parameter is not set.
    field_name: the name of the GET parameter which holds the url to which the
                user should be redirected (default 'next').
    """
    n = request.GET.get(REDIRECT_FIELD_NAME, None)
    if n is not None and is_safe_url(url=n, host=request.get_host()):
        return n
    return resolve_url(redirect_to)
