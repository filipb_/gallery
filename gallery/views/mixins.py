from django.contrib.auth.mixins import LoginRequiredMixin as BuiltinLoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect


class LoginRequiredMixin(BuiltinLoginRequiredMixin):
    """Include this mixin in a class based view to force the users to
    authenticate before accessing it.
    """
    login_url = reverse_lazy('gallery:auth:login')


class NoLoginRequiredMixin(object):
    """Include this mixin in a class based view to stop the authenticated users
    from accessing it.
    """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect('gallery:core:index')
        else:
            return super(NoLoginRequiredMixin, self).dispatch(request, *args, **kwargs)
