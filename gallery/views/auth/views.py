from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import FormView, UpdateView
from ...forms.auth import RegisterForm, LoginForm, ProfileForm, \
        PasswordChangeForm
from ..common import get_redirect_url
from ..mixins import NoLoginRequiredMixin, LoginRequiredMixin


class RegisterView(NoLoginRequiredMixin, FormView):
    template_name = 'gallery/auth/register.html'
    form_class = RegisterForm
    success_url = reverse_lazy('gallery:core:index')

    def form_valid(self, form):
        form.save()

        # Auto login after registration.
        user = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1']
        )
        if user is not None and user.is_active:
            login(self.request, user)

        return super(RegisterView, self).form_valid(form)


class LoginView(NoLoginRequiredMixin, FormView):
    template_name = 'gallery/auth/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('gallery:core:index')

    def get_success_url(self):
        return get_redirect_url(self.request, self.success_url)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(LoginView, self).form_valid(form)


class EditProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'gallery/auth/edit_profile.html'
    form_class = ProfileForm
    success_url = reverse_lazy('gallery:auth:edit_profile')

    def get_object(self):
        return self.request.user.userdata

    def form_valid(self, form):
        messages.success(self.request, 'Your profile has been saved.')
        return super(EditProfileView, self).form_valid(form)


class EditAccountView(LoginRequiredMixin, FormView):
    template_name = 'gallery/auth/edit_account.html'
    form_class = PasswordChangeForm
    success_url = reverse_lazy('gallery:auth:edit_account')

    def get_form_kwargs(self):
        kwargs = super(EditAccountView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        username = self.request.user.get_username()
        form.save()
        messages.success(self.request, 'Your password has been changed.')

        # Login the user back on this device. By default all old sessions are
        # invalidated when the password changes.
        user = authenticate(
            username=username,
            password=form.cleaned_data['new_password1']
        )
        if user is not None and user.is_active:
            login(self.request, user)

        return super(EditAccountView, self).form_valid(form)
