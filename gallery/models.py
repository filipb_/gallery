import functools
import os
from PIL import Image
from io import BytesIO
from django.db import models
from django.core import validators
from django.db.models.signals import pre_delete, post_save
from django.core.urlresolvers import reverse
from django.dispatch.dispatcher import receiver
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from .helpers import utc_now


class AutoUpdatedMixin(models.Model):
    """This mixin adds a field which is automatically set to the current
    timestamp every time an object is updated.

    This method is preferred to the auto_now and auto_now_add options because
    they often cause issues eg. related to the invalid timezones caused by
    misconfiguration of the Django project. Additionaly it is a very good idea
    to use timestamps localized in UTC, mainly because certain databases can't
    store timezone information (eg. SQLite) but also because it is considered to
    be customary.
    """
    updated = models.DateTimeField(null=True, default=None)

    def save(self, *args, **kwargs):
        if self.pk is not None:
            self.updated = utc_now()
        super(AutoUpdatedMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class AutoCreatedMixin(models.Model):
    """This mixin adds a field which is automatically set to the current
    timestamp when an object is initially created. See AutoUpdatedMixin.
    """
    created = models.DateTimeField(null=True, default=None)

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.created = utc_now()
        super(AutoCreatedMixin, self).save(*args, **kwargs)

    class Meta:
        abstract = True


# LatField is used to store latitude.
LatField = functools.partial(models.FloatField, validators=[
    validators.MinValueValidator(-90),
    validators.MaxValueValidator(90),
])


# LonField is used to store longitude.
LonField = functools.partial(models.FloatField, validators=[
    validators.MinValueValidator(-180),
    validators.MaxValueValidator(180),
])


class UserData(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    lat = LatField(null=True)
    lon = LonField(null=True)
    display_location = models.BooleanField(
        default=False,
        help_text=_('Specifies if the location should be displayed in your '
                    'profile.')
    )
    about_me = models.TextField(
        blank=True,
        help_text=_('Tell other users something about yourself.'),
        validators=[
            validators.MaxLengthValidator(1000),
        ]
    )

    def location_is_set(self):
        """This method returns True if the user has set his location."""
        return self.lat is not None and self.lon is not None

    def should_display_location(self):
        """This method returns True if the location should be displayed in this
        user's profile.
        """
        return self.display_location and self.location_is_set()


class Photo(AutoCreatedMixin, AutoUpdatedMixin, models.Model):
    user = models.ForeignKey(User)
    lat = LatField()
    lon = LonField()
    description = models.TextField(blank=True, validators=[
        validators.MaxLengthValidator(1000),
    ])
    image = models.ImageField(upload_to='photos/')

    def score(self):
        return self.photovote_set.count()

    def get_absolute_url(self):
        return reverse('gallery:core:photo', kwargs={'pk': self.id})


def current_photo_upload_to(instance, filename):
    """Determines the filename using the file's mime type."""
    if not os.path.splitext(filename)[1]:
        if hasattr(instance.image, 'temporary_file_path'):
            f = instance.image.temporary_file_path()
        else:
            if hasattr(instance.image, 'read'):
                f = BytesIO(instance.image.read())
            else:
                f = BytesIO(instance.image['content'])
        image = Image.open(f)
        mime = Image.MIME.get(image.format)
        extensions = {
            'image/png': '.png',
            'image/jpeg': '.jpg',
        }
        filename = '{photo_id}_{user_id}{extension}'.format(photo_id=instance.photo.pk,
                                                            user_id=instance.user.pk,
                                                            extension=extensions[mime])
    return 'current_photos/{filename}'.format(filename=filename)


class CurrentPhoto(AutoCreatedMixin, models.Model):
    user = models.ForeignKey(User)
    photo = models.ForeignKey(Photo)
    image = models.ImageField(upload_to=current_photo_upload_to)

    class Meta:
        unique_together = ('user', 'photo')

    def score(self):
        return self.currentphotovote_set.count()

    def get_absolute_url(self):
        return reverse('gallery:core:photo_compare',
                       kwargs={'pk': self.photo.id, 'compare':self.id})

    def json(self):
        return {
            'id': self.id,
            'url': self.get_absolute_url(),
        }


class Comment(AutoCreatedMixin, AutoUpdatedMixin, models.Model):
    user = models.ForeignKey(User)
    photo = models.ForeignKey(Photo)
    text = models.TextField(blank=True, validators=[
        validators.MaxLengthValidator(1000),
    ])


class PhotoVote(AutoCreatedMixin, models.Model):
    user = models.ForeignKey(User)
    photo = models.ForeignKey(Photo)

    class Meta:
        unique_together = ('user', 'photo')


class CurrentPhotoVote(AutoCreatedMixin, models.Model):
    user = models.ForeignKey(User)
    current_photo = models.ForeignKey(CurrentPhoto)

    class Meta:
        unique_together = ('user', 'current_photo')


@receiver(pre_delete, sender=Photo)
def photo_pre_delete(sender, instance, **kwargs):
    """Removes the associated image file from the HDD."""
    print(type(instance.image))
    instance.image.delete(False)


@receiver(pre_delete, sender=CurrentPhoto)
def current_photo_pre_delete(sender, instance, **kwargs):
    """Removes the associated image file from the HDD."""
    print(type(instance.image))
    instance.image.delete(False)


@receiver(post_save, sender=User)
def add_user_data(sender, instance, created, **kwargs):
    """Automatically creates a related UserData model."""
    if created:
        user_data = UserData(user=instance)
        user_data.save()
