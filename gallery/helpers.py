import datetime
import pytz


def utc_now():
    """Returns an aware datetime object representing the current time in UTC."""
    return pytz.utc.localize(datetime.datetime.utcnow())


def get_user_location(request):
    """If the user is authenticated and has set his location in his profile that
    location is returned. Otherwise a predicted location is returned or None if
    it can't be detemined at all.

    request: An HttpRequest object.
    """
    if request.user.is_authenticated() and \
       request.user.userdata.location_is_set():
        return {
            'lat': request.user.userdata.lat,
            'lon': request.user.userdata.lon,
        }
    return None
