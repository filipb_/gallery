from .helpers import get_user_location


def processor(request):
    """The context processor which adds required variables to the template
    context. This processor should be included in the project settings.
    """
    return {
        'location': get_user_location(request),
    }
