# Gallery

## Installation
To install this application in your project go through the following steps:

1. Include the urls in the project's `urlpatterns` list.

        from django.conf.urls import include, url
        from django.contrib import admin

        urlpatterns = [
            url(r'^', include('gallery.urls', namespace='gallery')),
            url(r'^admin/', admin.site.urls),
        ]

2. Add `gallery.apps.GalleryConfig` to the list of the installed apps in the
   project settings.

        INSTALLED_APPS = [
            ...
            'gallery.apps.GalleryConfig',
        ]

3. Set `MEDIA_ROOT`.

        MEDIA_ROOT = '/path/to/the/uploads/directory'

4. Set `MEDIA_URL` and `STATIC_URL` and **configure the web server to serve those
   files directly**.

        MEDIA_URL = '/files/'
        STATIC_URL = '/static/'

5. Set `ABSOLUTE_URL_OVERRIDES`.

        ABSOLUTE_URL_OVERRIDES = {
            'auth.user': lambda u: '/user/%s/' % u.id,
        }

5. Include the context processors in the project settings:

        TEMPLATES = [
            {
                ...
                'OPTIONS': {
                    'context_processors': [
                        ...
                        'gallery.context_processors.processor',
                    ],
                },
            },
        ]

## Development

### The database
It is necessary to create migrations (which are currently not included in the
repository) before running the `migrate` command:

    python manage.py makemigrations gallery

**The `makemigrations` command must be executed after every single change to
`gallery/models.py`**. Only then can you run the `migrate` command and start
the webserver:

    python manage.py migrate
    python manage.py runserver

To make things easier, if you want to create the database, populate it and start
the dev server run the following command:

    make dev

See `_misc/run_dev.sh` and `gallery/management/commands/init_db.py` for
details. Warning: this script removes `db.sqlite3` and `uploads/` present in
the current directory and doesn't run `makemigrations` automatically.

### Static files
In order to build the static files it is necessary to execute the following
command:

    make static

**This has to be done after every change to files located in
`gallery/static`.** The following programs are required for the script to
function correctly: [sass][1], [yui-compressor][2] and [coffee][3].

[1]: http://sass-lang.com/
[2]: https://github.com/yui/yuicompressor
[3]: http://coffeescript.org/

### pyflakes
A simple solution for removing 95% of bugs in the code.

    pip install --upgrade pyflakes
    pyflakes gallery
