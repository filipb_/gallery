#!/bin/bash

# Libs
mkdir -p libs

if [ ! -f libs/jquery.js ]; then
    echo "Downloading libs/jquery.js"
    curl http://code.jquery.com/jquery-2.2.1.min.js > libs/jquery.js
fi

if [ ! -f libs/jquery.timeago.js ]; then
    echo "Downloading libs/jquery.timeago.js"
    curl http://timeago.yarp.com/jquery.timeago.js > libs/jquery.timeago.js
fi

if [ ! -f libs/ol.css ]; then
    echo "Downloading libs/ol.css"
    curl http://openlayers.org/en/v3.16.0/css/ol.css > libs/ol.css
fi

if [ ! -f libs/ol.js ]; then
    echo "Downloading libs/ol.js"
    curl http://openlayers.org/en/v3.16.0/build/ol.js > libs/ol.js
fi

# CSS
cat libs/ol.css gallery/static/gallery/css/src/style.scss \
    | sass --stdin --scss \
    | yui-compressor --type css \
    > gallery/static/gallery/css/main.css

# JS
srcfiles=(libs/jquery.js libs/jquery.timeago.js libs/ol.js gallery/static/gallery/js/src/map.coffee gallery/static/gallery/js/src/forms.coffee gallery/static/gallery/js/src/main.coffee gallery/static/gallery/js/src/helpers.coffee gallery/static/gallery/js/src/editor.coffee gallery/static/gallery/js/src/comparison.coffee)
for i in ${!srcfiles[*]}
do
    tmpfiles[$i]=$(mktemp)
    if [[ ${srcfiles[$i]} == *coffee ]]
    then
        coffee -b -c -p ${srcfiles[$i]} > ${tmpfiles[$i]}
    else
        cat ${srcfiles[$i]} > ${tmpfiles[$i]}
    fi
done
cat ${tmpfiles[*]} | yui-compressor --type js > gallery/static/gallery/js/main.js
rm ${tmpfiles[*]}
